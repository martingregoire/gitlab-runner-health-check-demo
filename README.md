# GitLab Runner health check demo

The GitLab CI runner has a [health check](https://gitlab.com/gitlab-org/gitlab-runner/blob/2eab04a55afa45ac51166c332f6bcb26ee3ca3a9/commands/helpers/health_check.go) in place, which basically waits for a service to be ready to serve requests. This health check is implemented by reading the service IP address and port from the environment variables, then trying to create a connection to this address and port.

Unfortunately when a service exposes *two* ports, the GitLab CI runner *might* take the wrong one from the environment variables, and report that the service did not start properly:

(this is from a GitLab CI job log output)

```
Waiting for services to be up and running...

*** WARNING: Service runner-...-mysql-0 probably didn't start properly.

Health check error:
service "runner-...-mysql-0-wait-for-service" timeout
```

## Links

- [Issue for GitLab Runner](https://gitlab.com/gitlab-org/gitlab-runner/issues/4143)
